#define toggle485pin A15
#define LISTEN LOW
#define TALK HIGH
#define ADDRESS 100

int total, average, low;
char data[5]={
  1,2,3,4,5};
int counter,dataSend;

void init485bus(HardwareSerial theSerial,int pin,int baud);
void do485writeChunk(char data[],int addressSend);
void do485write(int pin,char data);
void do485readMaster();


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  init485bus(Serial2,toggle485pin,9600);
}

void loop() {
  
  if(counter>5)
  {
    //who are we updating next
    switch(dataSend)
    {
    case 1:
      //write data to the third to get a response
      do485writeChunk(data,1);        
      dataSend++;        //increment counter for which board to send
      break;
      //write data to the second to get a response
    case 2:
      do485writeChunk(data,2);
      dataSend++;        //increment counter for which board to send
      break;
      //write data to the third to get a response
    case 3:
      do485writeChunk(data,3);        
      dataSend++;        //increment counter for which board to send
      break;
    // if we ran through them all, change the data being send and wait for next update again
    case 4:
      for(int i=0;i<sizeof(data);i++)        //do randomness formating, not sure how this is gonna work :)
        if(data[i]>100)                //TRYING to keep it from floating outside of 255 (single byte size)
          data[i]/=2;                        //so chop it up
        else                            //else blow it up and change factors
          data[i]=(data[i]*2)+6;         
      dataSend=0;        //reset counter for which board to send
      break; 
    }
    
    delay(50);    //wait a second
    do485readMaster();     //hopefully we will hear it now, else do it in outside counter, just need to wait to output data till change?
    
    Serial.print("DataSend= ");
    Serial.println(dataSend);
    Serial.print("Total: ");
    Serial.println(total);
    Serial.print("Average: ");
    Serial.println(average);
    Serial.print("Low: ");
    Serial.println(low);
    
    counter=0;
    delay(1500);
  }
  else
  {  
    Serial.print("Counter(to 5): ");
    Serial.println(counter);
    counter++;
  }
  delay(1000);
}



void do485writeChunk(char _data[],int addressSend)
{
  do485write(toggle485pin,0x98);        //start byte
  do485write(toggle485pin,addressSend);    //send address  
  for(int i=0;i<sizeof(_data);i++)
    do485write(toggle485pin,_data[i]);
  do485write(toggle485pin,0xFF);      //stop byte
}

void do485readMaster()
{
  if (Serial2.available()>3)
  {
    if(Serial2.read()==ADDRESS)    //is the communication for us...of course its for us :)
    {
      switch(Serial2.read())  //which board is talking
      {
      case 1:
        total=Serial2.read();
        break;
      case 2:
        average=Serial2.read();        
        break;
      case 3:
        low=Serial2.read();        
        break;
      }
    }    
  }
}
void do485write(int pin,char data)
{
  digitalWrite(pin,TALK);
  delay(10);
  Serial.write(data);
  delay(10);
  digitalWrite(pin,LISTEN);
}

void init485bus(HardwareSerial theSerial,int pin,int baud)
{
  theSerial.begin(baud);
  pinMode(pin,OUTPUT);
  digitalWrite(pin,LISTEN);
}




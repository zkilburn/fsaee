

//definitions to help easy development and readability
#define toggle485pin 8 //A15
#define LISTEN LOW
#define TALK HIGH
#define ADDRESS 101
#define BYTESTART 0x98
#define STOPBYTE 0xFF
#define MASTER 100


//485 write and init methods
void init485bus(int pin,int baud);
void do485write(int pin,char data);

int low, total, incomming;

void setup() {
  init485bus(toggle485pin,9600);   //start the 485 bus serial stream and pinmode(listen)
}

void loop()  {
  //if there is something on the line
  if (Serial.available()>2)
  {
    //check to see if its intended for us
    if(Serial.read()==BYTESTART)
    {
      if (Serial.read()==ADDRESS)
      {
      low=255;
      //if so read in until stop byte
      while(Serial.available() && (incomming=Serial.read())!=STOPBYTE) //while (stuff is there to read, and its not equal to the stop byte) 
      {
        total+=incomming; 
        if (low>incomming)
           low=incomming;
      }
      do485write(toggle485pin,MASTER);        //write a header address(back to master)
      do485write(toggle485pin,ADDRESS);        //write a header address(back to master)
      if(ADDRESS==101)
        do485write(toggle485pin,total);                              //send a byte out from 485 (debugging is total of send data)  (depends on which board we are (constant define)  
      else if(ADDRESS==102)
        do485write(toggle485pin,total/2);    
      else if(ADDRESS==103)
        do485write(toggle485pin,low);
      }
      else    //this information is not for us
      {
        while(Serial.read()!=STOPBYTE);        //eat away at the packet until its done   
      }
    }
    else    //we didnt see the start of this packet.. destroy it and wait until its a start byte
      {
        while(Serial.peek()!=BYTESTART)    //be gentle, just take a peek to check
        {
          Serial.read();            //if its not what we want, destroy.
        }        
      }
  } 
}

void do485write(int pin,char data)
{
  digitalWrite(pin,TALK);
  delay(10);
  Serial.write(data);
  delay(10);
  digitalWrite(pin,LISTEN);
}

void init485bus(int pin,int baud)
{
  Serial.begin(baud);
  pinMode(pin,OUTPUT);
  digitalWrite(pin,LISTEN);
}



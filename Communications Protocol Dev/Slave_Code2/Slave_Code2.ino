void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(8,OUTPUT);
  digitalWrite(8,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(8,HIGH);
  delay(10);
  Serial.write(0x03);
  delay(10);
  digitalWrite(8,LOW);
  delay(2500);
}
